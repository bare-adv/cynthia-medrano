const breakpoints = {
  small: "500px",
  medium: "800px",
  large: "1200px",
  menu: 747,
}

export default breakpoints
