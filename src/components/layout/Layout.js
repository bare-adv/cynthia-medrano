import React from "react"
import styled from "styled-components"

// import app components
import { Header, Footer } from "components"

const Layout = props => {
  return (
    <Container>
      <Header />
      <Main>{props?.children}</Main>
      <Footer />
    </Container>
  )
}

export default Layout

const Container = styled.div`
  background: #f3faf9;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  min-height: 100vh;
`

const Main = styled.main``
