import React from "react"
import Helmet from "react-helmet"
import CssBaseline from "@material-ui/core/CssBaseline"
import { ThemeProvider } from "@material-ui/core/styles"
import "core-js/es6/number"
import "core-js/es6/array"

// import app components
import Layout from "./Layout"
import { SEO } from "components"
import { GlobalStyles } from "theme"
import { StoreProvider } from "store"

import mui from "theme/mui"

const App = props => {
  const { pageContext } = props

  return (
    <StoreProvider>
      <ThemeProvider theme={mui}>
        <Helmet>
          <html lang="en" />
          <meta name="description" />
        </Helmet>

        <SEO {...pageContext} />

        <CssBaseline />
        <GlobalStyles />

        <Layout {...props}>{props?.children}</Layout>
      </ThemeProvider>
    </StoreProvider>
  )
}

export default App
