import React from "react"

// import app components
import FormContainer from "components/form"

const Form = props => {
  return (
    <div className="grid-edges">
      <FormContainer {...props} />
    </div>
  )
}

export default Form
