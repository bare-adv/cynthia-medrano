# Gatsby Starter Bare

Links:

WP Gatsby + WP GraphQL
https://github.com/TylerBarnes/using-gatsby-source-wordpress-experimental/tree/master/WordPress/plugins

WP GraphQL ACF
https://github.com/wp-graphql/wp-graphql-acf/releases

WP GraphQL Yoast
https://github.com/ashhitch/wp-graphql-yoast-seo/releases

WP GraphiQL
https://github.com/wp-graphql/wp-graphiql/releases
