const CaseSensitivePathsPlugin = require("case-sensitive-paths-webpack-plugin")
const path = require("path")
const fs = require("fs")

exports.onCreateWebpackConfig = ({ actions }) => {
  actions.setWebpackConfig({
    plugins: [new CaseSensitivePathsPlugin()],
  })
}

require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
})

exports.onCreateWebpackConfig = ({ actions, stage }) => {
  if (stage === "build-html") {
    actions.setWebpackConfig({
      module: {
        rules: [
          {
            test: /react-mapbox-gl/,
            use: ["null-loader"],
          },
        ],
      },
    })
  }
}

const seoString = `
  seo {
    title
    metaDesc
    opengraphTitle
    opengraphDescription
    opengraphImage {
      id
      mediaItemUrl
      link
      sourceUrl
    }
  }
`

exports.createPages = async ({ actions, reporter, graphql }, pluginOptions) => {
  const {
    data: {
      wp: {
        generalSettings,
        readingSettings: { postsPerPage },
        themeOptions: {
          siteOptions: { frontendUrl },
        },
      },
    },
  } = await graphql(`
    query ROOT_FIELDS {
      wp {
        generalSettings {
          title
        }
        readingSettings {
          postsPerPage
        }
        themeOptions {
          siteOptions {
            frontendUrl
          }
        }
      }
    }
  `)

  const formattedFrontendUrl = frontendUrl.replace(/\/+$/, "")

  const allNodes = {}

  try {
    // Get all post types
    const {
      data: { allWpContentType },
    } = await graphql(/* GraphQL */ `
      query ALL_CONTENT_TYPES {
        allWpContentType {
          nodes {
            graphqlSingleName
          }
        }
      }
    `)

    for (const contentType of allWpContentType.nodes) {
      const { graphqlSingleName: postType } = contentType

      // Don't create single pages for media items
      if (postType === "mediaItem") {
        continue
      }

      // Capitalize post type name
      const nodesTypeName = postType.charAt(0).toUpperCase() + postType.slice(1)
      const gatsbyNodeListFieldName = `allWp${nodesTypeName}`

      const { data } = await graphql(/* GraphQL */ `
        query ALL_CONTENT_NODES {
            ${gatsbyNodeListFieldName}{
            nodes {
              databaseId
              id
              uri
              ${seoString}
              template {
                templateName
              }
            }
          }
        }
      `)

      allNodes[postType] = data[gatsbyNodeListFieldName].nodes
    }
  } catch (err) {
    if (err.response && err.response.data.message) {
      reporter.error(err.response.data.message)
    }
  }

  // Initialize missing templates object
  const missingTemplates = {}

  await Promise.all(
    Object.keys(allNodes).map(async postType => {
      await Promise.all(
        allNodes[postType].map(async (node, i) => {
          let {
            id,
            uri,
            template: { templateName },
            seo,
          } = node

          const isArchive = templateName.includes("Archive ")

          const archivePostType = templateName
            .replace("Archive ", "")
            .toLowerCase()

          let templatePath

          if (isArchive) {
            templatePath = path.resolve(
              `./src/templates/postTypes/${archivePostType}/archive.js`
            )
          } else {
            templatePath = path.resolve(
              `./src/templates/postTypes/${postType}/${templateName
                .toLowerCase()
                .replace(" ", "")}.js`
            )
          }

          if (fs.existsSync(templatePath)) {
            if (isArchive) {
              const numberOfPosts = allNodes[archivePostType].length
              const numberOfPages = Math.ceil(numberOfPosts / postsPerPage)

              for (let page = 1; page <= numberOfPages; page++) {
                let pathname = uri

                if (page > 1) {
                  pathname = `${uri}page/${page}`
                }

                pathname = pathname.replace(formattedFrontendUrl, "")

                actions.createPage({
                  component: templatePath,
                  path: pathname,
                  context: {
                    siteTitle: generalSettings.title,
                    id,
                    pagination: {
                      basePath: uri.replace(formattedFrontendUrl, ""),
                      numberOfPosts,
                      postsPerPage,
                      numberOfPages: Math.ceil(numberOfPosts / postsPerPage),
                      page,
                    },
                    seo,
                    nextPage: (allNodes[i + 1] || {}).id,
                    previousPage: (allNodes[i - 1] || {}).id,
                    frontendUrl: formattedFrontendUrl,
                  },
                })
              }
            } else {
              actions.createPage({
                component: templatePath,
                path: uri.replace(formattedFrontendUrl, ""),
                context: {
                  siteTitle: generalSettings.title,
                  id,
                  seo,
                  frontendUrl: formattedFrontendUrl,
                },
              })
            }
          } else {
            // Check if error was already shown
            if (!missingTemplates[templatePath]) {
              reporter.warn(
                `Template file not found. Gatsby won't create any pages for ${postType}/${templateName
                  .toLowerCase()
                  .replace(" ", "")}`
              )

              // Only show error message about missing template once
              missingTemplates[templatePath] = true
            }
          }
        })
      )
    })
  )

  // try {
  //   // Get all taxonomies
  //   const {
  //     data: { allWpTaxonomy },
  //   } = await graphql(/* GraphQL */ `
  //     query ALL_TAXONOMIES {
  //       allWpTaxonomy {
  //         nodes {
  //           graphqlSingleName
  //         }
  //       }
  //     }
  //   `)

  //   for (const taxonomy of allWpTaxonomy.nodes) {
  //     const { graphqlSingleName } = taxonomy

  //     // Don't create single pages for post format
  //     if (graphqlSingleName === "postFormat") {
  //       continue
  //     }

  //     // Capitalize post type name
  //     const nodesTypeName =
  //       graphqlSingleName.charAt(0).toUpperCase() + graphqlSingleName.slice(1)
  //     const gatsbyNodeListFieldName = `allWp${nodesTypeName}`

  //     const { data } = await graphql(/* GraphQL */ `
  //       query ALL_TERM_NODES {
  //           ${gatsbyNodeListFieldName}{
  //           nodes {
  //             id
  //             slug
  //             uri
  //           }
  //         }
  //       }
  //     `)

  //     await Promise.all(
  //       data[gatsbyNodeListFieldName].nodes.map(async (node, i) => {
  //         const templatePath = path.resolve(
  //           `./src/templates/taxonomies/${graphqlSingleName}/single.tsx`
  //         )

  //         if (fs.existsSync(templatePath)) {
  //           const { uri, slug, id } = node

  //           actions.createPage({
  //             component: templatePath,
  //             path: uri.replace(formattedFrontendUrl, ""),
  //             context: {
  //               siteTitle: generalSettings.title,
  //               id,
  //               slug,
  //               frontendUrl: formattedFrontendUrl,
  //             },
  //           })
  //         } else {
  //           // Check if error was already shown
  //           if (!missingTemplates[templatePath]) {
  //             reporter.warn(
  //               `Template file not found. Gatsby won't create any pages for taxonomy ${graphqlSingleName}`
  //             )

  //             // Only show error message about missing template once
  //             missingTemplates[templatePath] = true
  //           }
  //         }
  //       })
  //     )
  //   }
  // } catch (err) {
  //   if (err.response && err.response.data.message) {
  //     reporter.error(err.response.data.message)
  //   }
  // }
}
